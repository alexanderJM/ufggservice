/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.dao;

import com.mycompany.ufggservice.model.ComboComment;
import java.util.List;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
public interface CommentDao {

    ComboComment findById(int id);

    void saveComment(ComboComment comment);

    void updateComment(ComboComment comment);

    List<ComboComment> getCommentsByPage(int page,int comboId);   
}
