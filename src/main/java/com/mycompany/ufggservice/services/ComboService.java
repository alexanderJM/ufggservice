/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.services;

import com.mycompany.ufggservice.model.Combo;
import java.util.List;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
public interface ComboService {
    Combo getComboById(int id);
    List<Combo> getCombosByPage(int page);
    List<Combo> getCombosByCharacter(int page, int character);
    void saveCombo(Combo combos);
    void updateCombo(Combo combos);
    boolean isComboExist(Combo combo);
}
