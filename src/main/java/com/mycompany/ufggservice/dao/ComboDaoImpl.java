/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.dao;

import com.mycompany.ufggservice.model.Combo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Repository("comboDao")
public class ComboDaoImpl extends AbstractDao<Integer, Combo> implements ComboDao {

    @Override
    public Combo findById(int id) {
        return getByKey(id);
    }

    @Override
    public void saveCombo(Combo combo) {
        persist(combo);
    }

    @Override
    public void updateCombo(Combo combo) {
        update(combo);
    }

    @Override
    public List<Combo> getCombosByPage(int page) {
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.desc("postDate"));
        //criteria.setFetchMode("comboId", FetchMode.SELECT); 
        criteria.setMaxResults(10);
        if (page != 0) {
            criteria.setFirstResult(page * 10);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ComboDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return (List<Combo>) criteria.list();

    }

    @Override
    public List<Combo> getCombosByCharacter(int page, int character) {
        Criteria criteria = createEntityCriteria();
        //criteria.setFetchMode("combo", FetchMode.JOIN); 
        criteria.add(Restrictions.eq("gameCharacter.characterId", character));
        criteria.addOrder(Order.desc("postDate"));
        criteria.setMaxResults(10);
        if (page != 0) {
            criteria.setFirstResult(page * 10);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ComboDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return (List<Combo>) criteria.list();    }

}
