/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.controller;

import com.mycompany.ufggservice.model.Combo;
import com.mycompany.ufggservice.model.ComboComment;
import com.mycompany.ufggservice.services.ComboService;
import com.mycompany.ufggservice.services.CommentService;
import com.mycompany.ufggservice.utils.ComboUtils;
import com.mycompany.ufggservice.utils.Control;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@RestController
public class AppRestController {

    @Autowired
    ComboService comboService;
    @Autowired
    CommentService commentService;

    /*---------------------------------------------------------Combo Methods------------------------------------------------*/
    
    //-------------------Retrieve Combos By Page--------------------------------------------------------
    @RequestMapping(value = "/combos/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Combo>> listCombosByPage(@PathVariable("page") int page) {
        List<Combo> combos = comboService.getCombosByPage(page);
        if (combos.isEmpty()) {
            return new ResponseEntity<List<Combo>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Combo>>(combos, HttpStatus.OK);
    }
    
    //-------------------Retrieve Combos By Page By Character--------------------------------------------------------
    @RequestMapping(value = "/combos/{page}/{character}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Combo>> listCombosByCharacter(@PathVariable("page") int page,@PathVariable("character") int character) {
        List<Combo> combos = comboService.getCombosByCharacter(page,character);
        if (combos.isEmpty()) {
            return new ResponseEntity<List<Combo>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Combo>>(combos, HttpStatus.OK);
    }

    //-------------------Retrieve Single Combo--------------------------------------------------------
    @RequestMapping(value = "/combo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Combo> getCombo(@PathVariable("id") int id) {
        System.out.println("Fetching Combo with id " + id);
        Combo combo = comboService.getComboById(id);
        if (combo == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<Combo>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Combo>(combo, HttpStatus.OK);
    }

    //-------------------Create a Combo--------------------------------------------------------
    @RequestMapping(value = "/combo/", method = RequestMethod.POST)
    public ResponseEntity<Void> createCombo(@RequestBody Combo combo, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Combo " + combo.getCombo());
        if (combo.getCombo().trim() != "" && ComboUtils.checkCombo(combo.getCombo()) == true) {
            combo.setPostDate(System.currentTimeMillis());
            comboService.saveCombo(combo);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/combo/{id}").buildAndExpand(combo.getComboId()).toUri());
            return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
        
    }

    //------------------- Update a Combo --------------------------------------------------------
    @RequestMapping(value = "/combo/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Combo> updateCombo(@PathVariable("id") int id, @RequestBody Combo combo) {
        System.out.println("Updating Combo " + id);

        Combo currentCombo = comboService.getComboById(id);

        if (currentCombo == null) {
            System.out.println("Combo with id " + id + " not found");
            return new ResponseEntity<Combo>(HttpStatus.NOT_FOUND);
        }

        currentCombo.setComboId(combo.getComboId());
        currentCombo.setCombo(combo.getCombo());
        currentCombo.setPostDate(combo.getPostDate());

        comboService.updateCombo(currentCombo);
        return new ResponseEntity<Combo>(currentCombo, HttpStatus.OK);
    }
    
        /*---------------------------------------------------------Comment Methods------------------------------------------------*/
    //-------------------Retrieve Comments By Page--------------------------------------------------------
    @RequestMapping(value = "/comments/{page}/{comboId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ComboComment>> listCommentsByPage(@PathVariable("page") int page,@PathVariable("comboId") int comboId) {
        List<ComboComment> comments = commentService.getCommentsByPage(page,comboId);
        if (comments.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    //-------------------Retrieve Single Comment--------------------------------------------------------
    @RequestMapping(value = "/comment/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ComboComment> getComment(@PathVariable("id") int id) {
        System.out.println("Fetching User with id " + id);
        ComboComment comment = commentService.findById(id);
        if (comment == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<ComboComment>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ComboComment>(comment, HttpStatus.OK);
    }
    
    //-------------------Create a Comment--------------------------------------------------------
    @RequestMapping(value = "/comment/", method = RequestMethod.POST)
    public ResponseEntity<Void> createComment(@RequestBody ComboComment comment, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Comment " + comment.getComboComment());

        if(comment.getComboComment().trim() != ""){
        comment.setPostDate(System.currentTimeMillis());
        commentService.saveComment(comment);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/comment/{id}").buildAndExpand(comment.getCombo().getComboId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);

        }
        
    }

    //------------------- Update a Comment --------------------------------------------------------
    @RequestMapping(value = "/comment/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ComboComment> updateComment(@PathVariable("id") int id, @RequestBody ComboComment comment) {
        System.out.println("Updating Comment " + id);

        ComboComment currentComment = commentService.findById(id);

        if (currentComment == null) {
            System.out.println("Comment with id " + id + " not found");
            return new ResponseEntity<ComboComment>(HttpStatus.NOT_FOUND);
        }

        currentComment.setCombo(comment.getCombo());
        currentComment.setComboComment(comment.getComboComment());
        currentComment.setPostDate(comment.getPostDate());

        commentService.updateComment(currentComment);
        return new ResponseEntity<ComboComment>(currentComment, HttpStatus.OK);
    }
}
