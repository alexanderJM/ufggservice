/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.services;

import com.mycompany.ufggservice.dao.CommentDao;
import com.mycompany.ufggservice.model.ComboComment;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Service("CommentService")
@Transactional
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentDao dao;
    
    @Override
    public ComboComment findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void saveComment(ComboComment comment) {
        dao.saveComment(comment);
    }

    @Override
    public void updateComment(ComboComment comment) {
        dao.updateComment(comment);
    }

    @Override
    public List<ComboComment> getCommentsByPage(int page, int comboId) {
        return dao.getCommentsByPage(page, comboId);
    }
    
}
