# README #

### What is this repository for? ###

* This is a RESTful WEB Service for the UFGG android aplication. it's purpose is to GET and POST combos and comments for the aplication
* Version: still in early development

### How do I get set up? ###

* UFGGService is a Maven web application project made in NetBeans 8.1 with Spring 4.2.0 and Hibernate 5.2.2
* Configuration
* Dependencies: javaee-web-api,spring-orm,spring-webmvc, spring-tx, jackson-databind, jackson-datatype-hibernate5, javax.servlet-api, hibernate-entitymanager, hibernate-core, mysql-connector-java, json-simple, jettison, junit
* Database configuration: Database was made with Mysql, database tables:
create database if not exists UFGG;

use UFGG;


drop table if exists COMBO_COMMENT;
drop table if exists COMBO;
drop table if exists GAME_CHARACTER;
drop table if exists GAME;

create table GAME(
	ID int unsigned not null auto_increment,
	GAME varchar(255) not null,
	primary key (ID)
);


create table GAME_CHARACTER(
	ID int unsigned not null auto_increment,
	GAME_ID int unsigned not null,
    GAME_CHARACTER varchar(255) not null,
    primary key (ID),
	foreign key (GAME_ID) references GAME(ID)
);

create table COMBO
(
	ID int unsigned not null auto_increment,
    CHARACTER_ID int unsigned not null,
   	GAME_ID int unsigned not null,
	COMBO varchar(255) not null,
	POST_DATE int8 unsigned not null,
	DAMAGE int unsigned,
	COMBO_COUNT int unsigned,
	NUMBER_OF_METER int unsigned,
	primary key (ID),
    foreign key (GAME_ID) references GAME(ID),
    foreign key (CHARACTER_ID) references GAME_CHARACTER(ID)
);

create table COMBO_COMMENT
(
	ID int unsigned not null auto_increment,
    COMBO_ID int unsigned not null,
	COMBO_COMMENT varchar(255) not null,
	POST_DATE int8 unsigned not null,
	primary key (ID),
    key COMBO_FK (COMBO_ID),
    constraint COMBO_FK foreign key (COMBO_ID) REFERENCES COMBO(ID) on delete cascade on update cascade
);


* Deployment with Tomcat

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* alexanderenriquejm@gmail.com