/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.services;

import com.mycompany.ufggservice.dao.ComboDao;
import com.mycompany.ufggservice.model.Combo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Service("comboService")
@Transactional
public class ComboServiceImpl implements ComboService{

    @Autowired
    private ComboDao dao;

    @Override
    public Combo getComboById(int id) {
        return dao.findById(id);
    }

    @Override
    public List<Combo> getCombosByPage(int page) {
        return dao.getCombosByPage(page);
    }

    @Override
    public void saveCombo(Combo combo) {
        dao.saveCombo(combo);
    }

    @Override
    public void updateCombo(Combo combo) {
        dao.updateCombo(combo);
    }

    @Override
    public boolean isComboExist(Combo combo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Combo> getCombosByCharacter(int page, int character) {
        return dao.getCombosByCharacter(page,character);
    }
    
}
