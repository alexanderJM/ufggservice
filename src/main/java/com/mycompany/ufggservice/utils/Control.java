/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.utils;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
public enum Control {
    UP                                          (0),
    DOWN                                        (1),
    FORWARD                                     (2),
    BACK                                        (3),
    ONE                                         (4),
    TWO                                         (5),
    THREE                                       (6),
    FOUR                                        (7),
    JUMP                                        (8),
    JUMP_IN                                     (9),
    NEUTRAL_JUMP                                (10),
    FIRST_CANCEL                                (11),
    CANCEL                                      (12),
    EX                                          (13),
    METER_BURN                                  (14),
    RUN                                         (15),
    THROW                                       (16),
    END_CURRENT_STRING                          (17),
    PLUS                                        (18),
    STANDING                                    (19),
    CROUCHING                                   (20),
    AIR_DASH                                    (21),
    DIZZY                                       (22);
       
    private int buttonValue;
    
    Control(int buttonValue) {
        this.buttonValue = buttonValue;
        
    }

    public int getButtonValue() {
        return buttonValue;
    }

    public void setButtonValue(int buttonValue) {
        this.buttonValue = buttonValue;
    }
    
    public static Control getValue(int code) {
        for (Control items : Control.values()) {
            if (items.buttonValue == code) {
                return items;
            }
        }
        return null;
    }
}
