/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.dao;

import com.mycompany.ufggservice.model.ComboComment;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Repository("commentDao")
public class CommentDaoImpl extends AbstractDao<Integer, ComboComment> implements CommentDao {

    @Override
    public ComboComment findById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveComment(ComboComment comment) {
        persist(comment);
    }

    @Override
    public void updateComment(ComboComment comment) {
        update(comment);
    }

    @Override
    public List<ComboComment> getCommentsByPage(int page,int comboId) {
        //String hql = "from ComboComment where combo.comboId =:comboId";
        //Query query = getSession().createQuery(hql).setInteger("comboId", comboId);
       
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("combo.comboId", comboId));
        criteria.addOrder(Order.desc("postDate"));
        criteria.setMaxResults(10);
        if (page != 0) {
            criteria.setFirstResult(page * 10);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ComboDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        //ComboComment cc = (ComboComment)criteria.list().get(0);
        //System.out.println("TEEEEEEEEEEEST"+cc.getCombo().getComboId());
        return (List<ComboComment>) criteria.list();
    }
    
}
