/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Entity
@Table(name="COMBO")
public class Combo {
    
    
   
    private int comboId;
    
    private Game game;
    
    private GameCharacter gameCharacter;
    private String combo;
    private long postDate;
    private Integer damage;    
    private Integer comboCount;  
    private Integer numberOfMeter;  
   

    public Combo(){
    
    }
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="COMBO_ID",nullable = false)
    public int getComboId() {
        return comboId;
    }

    public void setComboId(int comboId) {
        this.comboId = comboId;
    }
    
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Game.class,cascade = CascadeType.MERGE)
    @JoinColumn(name = "COMBO_GAME_ID",nullable = false)
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = GameCharacter.class,cascade = CascadeType.MERGE)
    @JoinColumn(name = "CHARACTER_ID",nullable = false)  
    public GameCharacter getGameCharacter() {
        return gameCharacter;
    }

    public void setGameCharacter(GameCharacter gameCharacter) {
        this.gameCharacter = gameCharacter;
    }
    
    @Column(name="COMBO",nullable = false)
    public String getCombo() {
        return combo;
    }

    public void setCombo(String combo) {
        this.combo = combo;
    }
    
    @Column(name="POST_DATE",nullable = false)
    public long getPostDate() {
        return postDate;
    }

    public void setPostDate(long postDate) {
        this.postDate = postDate;
    }
    
    @Column(name="DAMAGE",nullable = true)
    public Integer getDamage() {
        return damage;
    }

    public void setDamage(Integer damage) {
        this.damage = damage;
    }
    
    @Column(name="COMBO_COUNT",nullable = true)
    public Integer getComboCount() {
        return comboCount;
    }

    public void setComboCount(Integer comboCount) {
        this.comboCount = comboCount;
    }
    
    @Column(name="NUMBER_OF_METER",nullable = true)
    public Integer getNumberOfMeter() {
        return numberOfMeter;
    }

    public void setNumberOfMeter(Integer numberOfMeter) {
        this.numberOfMeter = numberOfMeter;
    }
    
   
    
}
