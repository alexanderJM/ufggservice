/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.model;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Entity
@Table(name="GAME")
public class Game {
   
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="GAME_ID",nullable = false)
    private int gameId;
    @Column(name="GAME",nullable = false)
    private String game;
    public Game(){
    
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
   

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }
    
}
