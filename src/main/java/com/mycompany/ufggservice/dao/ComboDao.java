/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.dao;

import com.mycompany.ufggservice.model.Combo;
import java.util.List;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
public interface ComboDao {

    Combo findById(int id);

    void saveCombo(Combo combo);

    void updateCombo(Combo combo);

    List<Combo> getCombosByPage(int page);

    List<Combo> getCombosByCharacter(int page,int character);

}