/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Entity
@Table(name="COMBO_COMMENT")
public class ComboComment {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="COMMENT_ID",nullable = false)
    private int commentId;
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Combo.class,cascade = CascadeType.MERGE)
    @JoinColumn(name = "COMBO_ID",nullable = false,referencedColumnName="COMBO_ID")
    private Combo combo;
    @Column(name="COMBO_COMMENT",nullable = false)
    private String comboComment;
    @Column(name="POST_DATE",nullable = false)
    private long postDate;
    public ComboComment(){
    
    }

    
    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public Combo getCombo() {
        return combo;
    }

    public void setCombo(Combo combo) {
        this.combo = combo;
    }

    
    public String getComboComment() {
        return comboComment;
    }

    public void setComboComment(String ComboComment) {
        this.comboComment = ComboComment;
    }
    
    
    public long getPostDate() {
        return postDate;
    }

    public void setPostDate(long postDate) {
        this.postDate = postDate;
    }

}
