/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ufggservice.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexander Jimenez (alexanderenriquejm@gmail.com)
 */
@Entity
@Table(name="GAME_CHARACTER")
public class GameCharacter {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="CHARACTER_ID",nullable = false)
    private int characterId;
    //@Column(name="CHARACTER_GAME_ID",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Game.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "CHARACTER_GAME_ID",nullable = false)
    private Game characterGameId;
    @Column(name="GAME_CHARACTER",nullable = false)
    private String gameCharacter;
    public GameCharacter(){
    
    }

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public Game getCharacterGameId() {
        return characterGameId;
    }

    public void setCharacterGameId(Game characterGameId) {
        this.characterGameId = characterGameId;
    }
    
    public String getGameCharacter() {
        return gameCharacter;
    }

    public void setGameCharacter(String gameCharacter) {
        this.gameCharacter = gameCharacter;
    }
    
}
